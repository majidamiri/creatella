import request from './request';
import {GET_PRODUCTS_API} from "./endpoints";

export const getProducts = (_page, _sort) =>
    request(GET_PRODUCTS_API,{ _page, _sort });
