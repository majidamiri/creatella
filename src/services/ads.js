import {GET_ADS_API} from "./endpoints";

export const getAds = id => `${GET_ADS_API}/?r=${id}`