const baseUrl = 'http://localhost:3000'

export const GET_PRODUCTS_API = baseUrl + '/products';
export const GET_ADS_API = baseUrl + '/ads';