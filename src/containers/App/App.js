import React, {Fragment, useEffect, useState} from 'react';
import Card from "../../components/Card";
import {getProducts} from "../../services/products";
import Loading from "../../components/Loading";
import List from "../../components/List";
import Header from "../../components/Header";
import Sort from "../../components/Sort";
import useScroll from "../../components/Hooks/useScroll";
import '../../styles/creatella.scss';

function App() {
    const [products, setProducts] = useState({})
    const [productsEnd, setProductsEnd] = useState(false)
    const [loading, setLoading] = useState(false)
    const [currentPage, setPage] = useState(1)
    const [sortBy, setSortBy] = useState("size")
    const [ih, st, oh] = useScroll();

    // Fetch Products on component mount
    useEffect(() => {
        fetchProducts(currentPage, sortBy, true, true)
    }, [sortBy])

    // Fetch products in background
    useEffect(() => {
        fetchManager()
    }, [currentPage])

    // Change page and fetch products on scroll reaches the page end
    useEffect(() => {
        if (ih + st === oh && !loading && !productsEnd)
            setPage(prevPage => prevPage + 1)
    }, [st])

    // Set sort and reset page to 1
    function handleSort(val) {
        setSortBy(val)
        setPage(1)
    }

    // Fetch function
    async function fetchProducts(page, sort, withLoading, refreshData) {
        setProductsEnd(false)
        if (withLoading)
            setLoading(true)
        await getProducts(page, sort)
            .then(r => {
                if(r.data.length > 0) {
                    if (refreshData)
                        setProducts({[page]: r.data})
                    else
                        setProducts(prevProducts => ({...prevProducts, [page]: r.data}))
                } else {
                    setProductsEnd(true)
                }
            })
            .finally(() => withLoading ? setLoading(false) : undefined)
    }

    // Delays background fetches
    function fetchManager() {
        setTimeout(() => {
            if (!products[currentPage + 1])
                fetchProducts(currentPage + 1, sortBy)
        }, 2500)
    }

    return (
        <Fragment>
            <Header/>
            <div className="main-container">
                <div className="title-bar">
                    <h1>Products</h1>
                    <Sort sortBy={sortBy} onChange={handleSort}/>
                </div>
                <List
                    items={products}
                    page={currentPage}
                    productsEnd={productsEnd}
                    renderItem={item => <Card data={item} key={item.id}/>}
                    showAdsOffset={20}
                />
                <Loading show={loading}/>
            </div>
        </Fragment>
    );
}

export default App;
