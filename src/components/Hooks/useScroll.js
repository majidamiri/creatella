/**
 * Scroll listener hook
 * @return {Array} [window.innerHeight, document.documentElement.scrollTop, document.documentElement.offsetHeight]
 **/

import {useState} from 'react';
import useEventListener from '@use-it/event-listener';
import {debounce} from "../../utils/helpers";

const useScroll = () => {
    const [positions, setPositions] = useState([window.innerHeight, document.documentElement.scrollTop, document.documentElement.offsetHeight]);
    const handleScroll = debounce(e => {
        setPositions([window.innerHeight, document.documentElement.scrollTop, document.documentElement.offsetHeight])
    }, 250)
    useEventListener('scroll', handleScroll);
    return positions;
};

export default useScroll;
