import React from 'react';
import HeaderStyles from './header.module.scss'

export default function Header() {
    return(
        <div className={HeaderStyles.header}>
            <h1>Creatella</h1>
        </div>
    )
}