import React, {Fragment, useState, useEffect} from 'react';
import Grid from "../Grid";
import Ads from "../Ads";

export default function List({renderItem, items, page, showAdsOffset, productsEnd}) {
    const [data, setData] = useState([])
    useEffect(() => {
        const data = []
        Object.keys(items).map(p => p <= page && data.push(...items[p]))
        setData(data)
    }, [items, page])
    return (
        <Grid>
            {data.map((item,index) => {
                if((index+1) % showAdsOffset === 0)
                    return(
                        <Fragment key={`with-ads-${index}`}>
                            {renderItem(item)}
                            <Ads id={index} key={`ads-${index}`}/>
                        </Fragment>
                    )
                return renderItem(item)
            })}
            {productsEnd && <h2>No more products...</h2>}
        </Grid>
    )
}