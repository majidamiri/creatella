import React from 'react'
import {getAds} from "../../services/ads";
import rndGenerator from "../../utils/rndGenerator";
import AdsStyle from './ads.module.scss'

export default function Ads({ id }) {
    const rndId = rndGenerator(id)
    return (
        <div className={AdsStyle.ads}>
            <img alt="Ads" src={getAds(rndId)}/>
            <div className={AdsStyle.text}>
                <p>Sponsor!</p>
            </div>
        </div>
    )
}