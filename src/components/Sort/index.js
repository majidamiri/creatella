import React from 'react';

const VALUES = [
    { value: "size", label: "Size" },
    { value: "price", label: "Price" },
    { value: "id", label: "ID" },
]

export default function Sort({ sortBy, onChange }) {
    return (
        <div>
            <span style={{ marginRight: 8 }}>Sort by</span>
            <select value={sortBy} onChange={e => onChange(e.target.value)}>
                {VALUES.map(o => <option key={o.value} value={o.value}>{o.label}</option>)}
            </select>
        </div>
    );
}