import React from 'react';
import CardStyles from './card.module.scss'
import index from "../../utils/timeFormatter";

export default function Card({data}) {
    return (
        <div className={CardStyles.card}>
            <div className={CardStyles.container}>
                <div className={CardStyles.face}>
                    <span style={{fontSize: data.size}}>{data.face}</span>
                </div>
                <div className={CardStyles.bottom}>
                    <div className={CardStyles.details}>
                        <h1>$0.{data.price}</h1>
                        <p>Added {index(data.date)} ago</p>
                    </div>
                </div>
            </div>
        </div>
    )
}