import React from 'react';
import LoadingStyles from './loading.module.scss'

export default function Loading({show}) {
    if (!show) return null;
    return (
        <div className={LoadingStyles.loading}>
            <div className={LoadingStyles.spinner}>
                <p>Loading...</p>
                <div className={LoadingStyles.bounce1}/>
                <div className={LoadingStyles.bounce2}/>
                <div className={LoadingStyles.bounce3}/>
            </div>
        </div>
    )
}