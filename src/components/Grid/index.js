import React from "react";
import GridStyles from './grid.module.scss'

export default function Grid({ children }) {
    return(
        <div className={GridStyles.gridContainer}>
            {children}
        </div>
    )
}