/**
 * Format Date as time expressions (Currently it only supports dates in the past)
 * @param  {Date} date A valid date object or string
 * @return {String} The time expression of given date
**/

export default function index(date) {
    if (!date) {
        return null;
    }
    const d = new Date(date);
    if(d.toString() === 'Invalid Date')
        return 'Invalid Date'
    const today = new Date();
    const seconds = Math.round((today - d) / 1000);
    const minutes = Math.round(seconds / 60);
    const hours = Math.round(minutes / 60);
    const days = Math.round(hours / 24);
    if (seconds < 5) {
        return 'now';
    } else if (seconds < 60) {
        return `${ seconds } seconds ago`;
    } else if (seconds < 90) {
        return 'about a minute ago';
    } else if (minutes < 60) {
        return `${ minutes } minutes ago`;
    } else if (hours < 24) {
        return `${ hours } hour(s) ago`;
    } else if (hours >= 24) {
        return `${ days } day(s) ago`;
    }
}