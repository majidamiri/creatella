import React from 'react';
import timeFormatter from './index';

describe("timeFormatter", () => {
    it("should have a default export", () => {
        expect(typeof timeFormatter).toBe("function");
    });
    it('should return null if date param is not passed', () => {
        const tf = timeFormatter();
        expect(tf).toBe(null);
    });
    it('should return (Invalid Date) if date passed is not valid', () => {
        const tf = timeFormatter('Blah Blah');
        expect(tf).toBe('Invalid Date');
    });
    it('should return (now) if (new Date()) is passed', () => {
        const tf = timeFormatter(new Date());
        expect(tf).toBe('now');
    });
    it('should return (3 day(s) ago) with passed date', () => {
        const todayDate = new Date().getDate();
        const date = new Date().setDate(todayDate - 3)
        const tf = timeFormatter(date);
        expect(tf).toBe('3 day(s) ago');
    });
})