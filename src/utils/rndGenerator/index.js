/**
 * Returns a random number based on the input
 * @param  {Number} number The input that the random number is made on
 * @return {Number} Generated random number
**/

export default function rndGenerator(number = 0) {
    return new Date().getUTCMilliseconds() + number
}