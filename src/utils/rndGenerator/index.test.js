import React from 'react';
import ReactDOM from 'react-dom';
import rndGenerator from '../rndGenerator';

describe("rndGenerator", () => {
    it('should return a number without params', () => {
        const rnd = rndGenerator();
        expect(rnd).toBeDefined();
    });
    it('should return a number with params', () => {
        const smallRnd = Math.floor(Math.random() * 10) + 1
        const rnd = rndGenerator(smallRnd);
        expect(rnd).toBeDefined();
    });
    it('should not return repeated value if params are defined', () => {
        const loopCount = 100;
        const arr = []
        for(let i = 0; i < loopCount; i++){
            const rnd = rndGenerator(i);
            arr.push(rnd)
        }
        const result = arr.some((item, index) => arr.indexOf(item) !== index)
        expect(result).toBe(false);
    });
})