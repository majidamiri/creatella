Running the Project
----
The project has main two parts.

- Frontend: This part is the react.js project that is based on `create-react-app` and runs on port `3001` (Used `cross-env` module to handle port change). You can simply run it with `yarn start`.
- Server or backend: Json-server part that runs on port `3000`. Run it with `yarn start:json-server`.

Project Structure
----
The backend structure is as you considered, so I just mention some notes about the frontend part.

- The frontend part is created based on `create-react-app` and all the files are included in `/src` directory.
- `Axios` lib is used for requests.
- I used `sass` and `scss-modules` for styling.
- Some tests are provided to ensure the main functions functionality.
- `/components` dir includes the main components of the app. Each component has its style module beside itself.
- `/containers` dir includes the pages or main wrappers of the app.
- `/services` dir includes all the stuff that is related to requests to backend.
- `/styles` includes the general styles of the project.
- `/utils` includes some useful functions and helpers that mostly handles some logic.
- There is nothing weird in the code. I just tried to make comments wherever possible.

Tests
----
`yarn test`